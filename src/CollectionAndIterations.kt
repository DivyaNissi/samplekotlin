fun sayHello(greeting:String, itemToGreet:List<String>) {
    itemToGreet.forEach {itemToGreet ->
        println("$greeting $itemToGreet")
    }
}

fun main() {

    val interestingThings = arrayOf("Kotlin", "Programming", "comic Books")
    println(interestingThings.size)
    println(interestingThings[0])
    println(interestingThings.get(1))
    //*** Normal for loop ***/
    for (interestingThing in interestingThings) {
       // println(interestingThing)
    }
    //***  Foreach ***/
    /* Invoking a foreach function that is available in this standard library. That function then takes into another functions returns Unit.*/
    interestingThings.forEach {
        // IT-> It is the default name for each elements in the array that passed into the function.
      //  println(it)
    }

    //*** you can change the name of it ***/
    interestingThings.forEach { intrestingThingIs ->
      //  println(intrestingThingIs)
    }
    //*** you can pass the index also *****/
    interestingThings.forEachIndexed {index, intrestingThingIs ->
        //println("$intrestingThingIs is at index of $index")
    }

    // ***** List off *** //

    val listArray = listOf("Kotlin", "Programming", "comic Books")
    listArray.forEach {listIs ->
        println("List Is..... $listArray")
    }
    // Essentially takes in pairs :
    val map = mapOf(1 to "a", 2 to "b", 3 to "c")
    map.forEach { key, value ->
        println("$key -> $value")
        // we seen I you can define several type of collections such as arrays list and maps.
        // and you can iteration over the collection and access individual elements from the collection.
        // Kotlin Handle the collections and  similar to the way and which diff like noable and no
        // By default a collection in kotlin is immutable. muta
    }
        val iterate = mutableListOf("Kotlin", "Programming", "comic Books")
        iterate.add("General books")
        println(iterate)
       /* val map1 = mutableMapOf(1 to "a", 2 to "b", 3 to "c")
        map1.forEach { key1, value1 ->
            println("$key1 -> $value1")
            map1.put(4, "d")
          }*/

    // call function here....

    sayHello("Hai",listArray)
}