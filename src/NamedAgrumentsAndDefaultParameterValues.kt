// varang key
fun sayHello1(greeting:String, vararg itemToGreet:String) {
    itemToGreet.forEach {itemToGreet ->
        println("$greeting $itemToGreet")
    }
}

fun greetPerson(greeting: String, name:String) = println("$greeting $name")

fun greetPerson1(greeting: String = "Hello", name:String = "Baby") = println("$greeting $name")


fun main() {
    val listArray = listOf("Kotlin", "Programming", "comic Books")
    sayHello1("Hai", "Kotlin", "Programming")

    // Another type
    // Variable when you pass it in as argument v, namalue.
    // so now if we hit the run , we will see that
    // The Compiler is now accepting that array and we are iterating over each item in that list array
    val listArray1 = arrayOf("Kotlin", "Programming", "comic Books")
    sayHello1("Hai", *listArray1)
    // Note: So this is how you can pass in an existing collection is a VAR ARD Parameter

    // ****** Named Arguments ******

    greetPerson("Hello","World")

    // It looks like defining the name of the parameter and then an equal sign.
    // And then, here we can say main equals. high to greeting and nissi
    greetPerson(greeting = "Hi",name = "Nissi")
    // we can actually pass the second parameter first and the first parameter second.
    // So that we could actually theoretically modify the signature of the greeting.
    greetPerson(name = "Nissi", greeting = "Hi")

    // we can use like this also
    greetPerson1(name = "Nissi")
}
