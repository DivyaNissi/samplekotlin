
val name = "Divya"
var gretting: String? = null

fun main() {
    //*** If statement
    gretting = "Hello"
    var greetingToPrint = if (gretting != null) gretting else "Hai"
    println(greetingToPrint)
    println(name)

    //When expression -> can be use  To assign a value to varible depending on the encript value.
    var greetingToPrint1 = when (gretting) {
        null -> "Hi"
        else -> gretting
    }
    println(greetingToPrint1)
    println(greetingToPrint)
    println(name)
}