
fun getGreeting(): String {
    return "Hello Kotlin"
}

// single expression functions
fun getGreeting1(): String = "Hello New"

// unit -> we don't want to retun eny type
fun sayHello(): Unit {
   println("Hello Divya")
}
fun sayHai(itemToGreet: String) {
    val msg = "Hello " + itemToGreet
    //or
    val msg1 = "Hello $itemToGreet"
    println(msg1)
    println(msg)
    //or
    println("Hello Geat $itemToGreet")
}

fun heloGreat(greeting:String, itemToGreet: String) = println("$greeting $itemToGreet")

fun main() {
    println("Hello world")
    println(getGreeting())
    println(getGreeting1())
    sayHello()
    sayHai("HELLO")
    heloGreat("I am","Divya")
}